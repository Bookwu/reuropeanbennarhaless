
old_bulwari_canals = {
	picture = "estuary_icon"
}
bulwari_canals = {
	province_trade_power_value = 2
	trade_steering = 0.02
	picture = "estuary_icon"
}
bulwari_locks_and_lifts = {
	province_trade_power_value = 5
	trade_steering = 0.05
	picture = "estuary_icon"
}
great_bulwari_watercourse = {
	province_trade_power_value = 10
	trade_steering = 0.10
	picture = "estuary_icon"
}
bulwari_tech_boom = {
    technology_cost = -0.05
    idea_cost = -0.05
    local_development_cost = -0.1
}

#Golden Highway
golden_highway = {
	local_friendly_movement_speed = 0.25
	supply_limit = 4
	province_trade_power_modifier = 0.1
	local_institution_spread = 0.25
	picture = "golden_highway_complete"
}
golden_highway_stage_1 = {
	supply_limit = 3
	local_friendly_movement_speed = 0.1
	picture = "golden_highway_one"
}
golden_highway_stage_2 = {
	supply_limit = 4
	local_friendly_movement_speed = 0.2
	local_institution_spread = 0.1
	picture = "golden_highway_two"
}
golden_highway_under_construction = {
	local_production_efficiency = -0.2
	picture = "golden_highway_construction"
}
golden_highway_construction_stalled = {
	picture = "golden_highway_construction_paused"
}

#Ruins
ruined_ekluzagnu = {
	local_defensiveness = 0.10
	picture = "ekluzagnu_ruined"
}

rebuilding_ekluzagnu = {
	local_tax_modifier = -0.5
	local_production_efficiency = -0.5
}

restored_ekluzagnu_palace = {
	local_defensiveness = 0.10
	local_development_cost = -0.50
	supply_limit = 5
	picture = "ekluzagnu_restored"
}

restored_ekluzagnu_fortress = {
	local_defensiveness = 0.25
	local_development_cost = -0.10
	supply_limit = 5
	picture = "ekluzagnu_restored"
}

#Surani Refugees

surani_refugee_large = {
	local_unrest = 1
	local_development_cost = -0.05
	local_manpower_modifier = 0.05
}

surani_refugee_medium = {
	local_unrest = 0.5
	local_development_cost = -0.025
	local_manpower_modifier = 0.025
}

surani_refugee_small = {
	local_unrest = 0.5
	local_manpower_modifier = 0.025
}

rejected_surani_province_modifier = {
	local_unrest = -1
	local_development_cost = -0.05
}

#Flavour Events
bulwar_umaslu = {
	tolerance_own = 1
	stability_cost_modifier = -0.10
}

bulwar_lost_caravan = {
	caravan_power = -0.15
}

bulwar_flavour_great_harvest = {
	trade_goods_size_modifier = 0.25
}

bulwar_flavour_no_helf = {
	tolerance_own = 1
}

bulwar_flavour_executed_helf = {
	tolerance_own = 2
	tolerance_heretic = -1
	tolerance_heathen = -1
}

exemplars_fight_eduz_nabari = {
	local_unrest = 2
}

judges_ho_goods = {
	local_production_efficiency = 0.1
}

judges_ho_unrest = {
	local_unrest = -2
}

#province modifiers related to Adventurers Wanted (formerly infestations) - try to keep them grouped together

aw_test_1 = {

}

aw_test_2 = {
	
}


aw_test_3 = {
	
}


aw_thieves_test_1 = {

}

aw_thieves_test_2 = {

}

aw_thieves_test_3 = {

}

aw_thieves_guild_1 = {
	local_tax_modifier = -0.1
	development_cost = 0.1

	available_province_loot = -0.25
}

aw_thieves_guild_2 = {
	local_tax_modifier = -0.25
	development_cost = 0.2

	available_province_loot = -0.5	#funnily enough, thieves guild will actually reduce loot for attackers to steal
}

aw_thieves_guild_3 = {
	local_tax_modifier = -0.5
	development_cost = 0.3
	global_tax_modifier = -0.01	#get fucked. lets go big with these AW, so try not to stack these!

	available_province_loot = -0.75
}

aw_thieves_guild_stolen_relics = {
	global_trade_goods_size_modifier = -0.5
}

aw_thieves_guild_war_among_thieves = {
	local_tax_modifier = -0.5
}


aw_bandits_1 = {
	local_monthly_devastation = 0.1
	province_trade_power_modifier = -0.1
}

aw_bandits_2 = {
	local_monthly_devastation = 0.2
	province_trade_power_modifier = -0.2
}

aw_bandits_3 = {
	local_monthly_devastation = 0.4
	province_trade_power_modifier = -0.3

	min_local_autonomy = 50
}


aw_bandits_tax_collectors_robbed = {
	local_tax_modifier = -0.25
	local_unrest = 5
}
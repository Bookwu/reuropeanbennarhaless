# 
setup_vision = yes
government = republic
add_government_reform = oligarchy_reform
government_rank = 1
primary_culture = zanite
add_accepted_culture = sun_elf
religion = bulwari_sun_cult
technology_group = tech_bulwari
capital = 601

1000.1.1 = {
	set_estate_privilege = estate_church_indepedent_clergy 
	add_isolationism = -1
	set_country_flag = nsc_human_country_magic_prohibition
}
